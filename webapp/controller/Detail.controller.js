sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "sap/ui/Device",
    "sap/m/MessageToast",
  ],
  function (
    BaseController,
    JSONModel,
    formatter,
    mobileLibrary,
    Device,
    UIComponent,
    MessageToast
  ) {
    "use strict";
    var currentFile = null;
    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("com.stulz.zfiwfapv.controller.Detail", {
      formatter: formatter,

      /* =========================================================== */
      /* lifecycle methods                                           */
      /* =========================================================== */

      onInit: function () {
        // Model used to manipulate control states. The chosen values make sure,
        // detail page is busy indication immediately so there is no break in
        // between the busy indication for loading the view's meta data

        var oViewModel = new JSONModel({
          busy: false,
          delay: 0,
        });

        this.getRouter()
          .getRoute("object")
          .attachPatternMatched(this._onObjectMatched, this);

        this.setModel(oViewModel, "detailView");

        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(this._onMetadataLoaded.bind(this));
        // this.oRouter = UIComponent.getRouterFor(this.getView());
        /*
        var oUploadCollection = this.getView().byId('UploadCollection');
        var oModelData1 = this.getOwnerComponent().getModel();
        oUploadCollection.setUploadUrl(oModelData1+"/DocumentoSet");
        var oModel = new sap.ui.model.odata.ODataModel(oModelData1,false);
        this.getView().setModel(oModel);
*/
      },

      /* =========================================================== */
      /* event handlers                                              */
      /* =========================================================== */

      /**
       * Event handler when the share by E-Mail button has been clicked
       * @public
       */
      onSendEmailPress: function () {
        var oViewModel = this.getModel("detailView");

        URLHelper.triggerEmail(
          null,
          oViewModel.getProperty("/shareSendEmailSubject"),
          oViewModel.getProperty("/shareSendEmailMessage")
        );
      },

      changeSelectedLine: function (oEvent) {
        console.log("onPress");

        // The source is the list item that got pressed
        this._showObject(oEvent.getSource());
      },
      _showObject: function (obj) {
        if (obj.getBindingContext().getProperty("Gjahrl") == "0000") {
          console.log("Prima nota");
          //prima nota
          this.getRouter().navTo("primaNota", {
            Belnr: obj.getBindingContext().getProperty("Belnr"),
            Gjahr: obj.getBindingContext().getProperty("Gjahr"),
            Bukrs: obj.getBindingContext().getProperty("Bukrs"),
            Buzei: obj.getBindingContext().getProperty("Buzei"),
          });
        } else {
          //verifica fattura
          console.log("verifica fattura");

          this.getRouter().navTo("verificaFattura", {
            Belnr: obj.getBindingContext().getProperty("Belnr"),
            Gjahr: obj.getBindingContext().getProperty("Gjahr"),
            Bukrs: obj.getBindingContext().getProperty("Bukrs"),
            Buzei: obj.getBindingContext().getProperty("Buzei"),
          });
        }
      },
      //file

      onSelectionChange: function () {
        var e = this.byId("UploadCollection");
        if (e.getSelectedItems().length > 0) {
          this.byId("downloadButton").setEnabled(true);
        } else {
          this.byId("downloadButton").setEnabled(false);
        }
      },
      onChange: function (e) {
        //     this.byId("UploadCollection").removeAllItems();
        var t = new FileReader();
        var a = e.getParameter("files")[0];
        console.log(a);
        this.fileName = a.name;
        t.onload = function (e) {
          currentFile = {
            fileName: a.name,
            content: e.target.result,
          };
          console.log(currentFile);
        };
        t.onerror = function () {
          sap.m.MessageToast.show("Error occured when uploading file");
          currentFile = null;
        };
        t.readAsArrayBuffer(a);
      },

      onDownloadItem: function (event) {
        var that = this;

        console.log(event.getSource().getBindingContext().getObject());
        var Bukrs = event.getSource().getBindingContext().getObject().Bukrs;
        var Livello = event.getSource().getBindingContext().getObject().Livello;
        var Belnr = event.getSource().getBindingContext().getObject().Belnr;
        var Gjahr = event.getSource().getBindingContext().getObject().Gjahr;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;
        var Ext = event.getSource().getBindingContext().getObject().Ext;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;
        console.log(
          "/DocumentoSet(Bukrs='" +
            Bukrs +
            "',Livello='" +
            Livello +
            "',Belnr='" +
            Belnr +
            "',Gjahr='" +
            Gjahr +
            "',ArcDocId='" +
            ArcDocId +
            "')"
        );

        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.read(
          "/DocumentoSet(Bukrs='" +
            Bukrs +
            "',Livello='" +
            Livello +
            "',Belnr='" +
            Belnr +
            "',Gjahr='" +
            Gjahr +
            "',ArcDocId='" +
            ArcDocId +
            "')",
          {
            success: function (oRetrievedResult) {
              console.log(oRetrievedResult.Datadoc);
                console.log(Ext);
              var b64Data = oRetrievedResult.Datadoc;

              var contentType = "";

              switch (Ext) {
                case "DOCX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                  break;
                case "DOC":
                  contentType = "application/msword";
                  break;
                case "XLSX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                  break;
                case "XLS":
                  contentType = "application/vnd.ms-excel";
                  break;
                case "PPT":
                  contentType = "application/vnd.ms-powerpoint";
                  break;
                case "PPTX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                  break;
                case "PNG":
                  contentType = "image/png";
                  break;
                case "JPEG":
                  contentType = "image/png";
                  break;
                case "JPG":
                  contentType = "image/png";
                  break;
                case "SVG":
                  contentType = "image/svg+xml";
                  break;
                case "PDF":
                  contentType = "application/pdf";
                  break;
                case "HTM":
                  contentType = "text/html";
                  break;
                case "HTML":
                  contentType = "text/html";
                  break;

                  default:
                  contentType = "application/*";
                  break;
              }
              
            var a =
                "data:" +
                contentType +
                ";base64," +
                oRetrievedResult.Datadoc +
                "";
              var o = document.createElement("a");
              var i = oRetrievedResult.ArcDocId;
              o.href = a;
              o.download = i;
              o.click();

              
            },
            error: function (oError) {
              console.log(oError);
            },
          }
        );
      },

      onDownloadItemSmart2: function (event) {
        var that = this;

        console.log(event.getSource().getBindingContext().getObject());
        var Bukrs = event.getSource().getBindingContext().getObject().Bukrs;
        var Livello = event.getSource().getBindingContext().getObject().Livello;
        var Belnr = event.getSource().getBindingContext().getObject().Belnr;
        var Gjahr = event.getSource().getBindingContext().getObject().Gjahr;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;
        var Ext = event.getSource().getBindingContext().getObject().Ext;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;
        console.log(
          "/DocumentoSet(Bukrs='" +
            Bukrs +
            "',Livello='" +
            Livello +
            "',Belnr='" +
            Belnr +
            "',Gjahr='" +
            Gjahr +
            "',ArcDocId='" +
            ArcDocId +
            "')"
        );

        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.read(
          "/DocumentoSet(Bukrs='" +
            Bukrs +
            "',Livello='" +
            Livello +
            "',Belnr='" +
            Belnr +
            "',Gjahr='" +
            Gjahr +
            "',ArcDocId='" +
            ArcDocId +
            "')",
          {
            success: function (oRetrievedResult) {
              console.log(oRetrievedResult.Datadoc);
                console.log(Ext);
              var b64Data = oRetrievedResult.Datadoc;

              var contentType = "";

              switch (Ext) {
                case "DOCX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                  break;
                case "DOC":
                  contentType = "application/msword";
                  break;
                case "XLSX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                  break;
                case "XLS":
                  contentType = "application/vnd.ms-excel";
                  break;
                case "PPT":
                  contentType = "application/vnd.ms-powerpoint";
                  break;
                case "PPTX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                  break;
                case "PNG":
                  contentType = "image/png";
                  break;
                case "JPEG":
                  contentType = "image/png";
                  break;
                case "JPG":
                  contentType = "image/png";
                  break;
                case "SVG":
                  contentType = "image/svg+xml";
                  break;
                case "PDF":
                  contentType = "application/pdf";
                  break;
                case "HTM":
                  contentType = "text/html";
                  break;
                case "HTML":
                  contentType = "text/html";
                  break;

                  default:
                  contentType = "application/*";
                  break;
              }
              
                var blob = that.b64toBlob(b64Data, contentType);
                var blobUrl = URL.createObjectURL(blob);
                window.open(blobUrl);
              
            },
            error: function (oError) {
              console.log(oError);
            },
          }
        );
      },
      b64toBlob: function (b64Data, contentType) {
        var sliceSize = 512;
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (
          let offset = 0;
          offset < byteCharacters.length;
          offset += sliceSize
        ) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
      },
     

      onDownloadItemNO: function () {
        var e = this.byId("UploadCollection");
        console.log(e);
        var t = e.getSelectedItems();
        console.log(t[0].mProperties.documentId);
        var model = this.getOwnerComponent().getModel();

        var a = this.getView().getBindingContext();
        var i = a.getPath();
        console.log(i);
        var currentObject = a.getModel().getProperty(i);
        if (t) {
          for (var n = 0; n < t.length; n++) {
            console.log(
              "/DocumentoSet(Bukrs='" +
                currentObject.Bukrs +
                "',Livello='" +
                currentObject.Livello +
                "',Belnr='" +
                currentObject.Belnr +
                "',Gjahr='" +
                currentObject.Gjahr +
                "',ArcDocId='" +
                t[n].mProperties.documentId +
                "')"
            );
            model.read(
              "/DocumentoSet(Bukrs='" +
                currentObject.Bukrs +
                "',Livello='" +
                currentObject.Livello +
                "',Belnr='" +
                currentObject.Belnr +
                "',Gjahr='" +
                currentObject.Gjahr +
                "',ArcDocId='" +
                t[n].mProperties.documentId +
                "')",
              {
                success: function (resp) {
                  console.log(resp);

                  var a =
                    "data:application" +
                    resp.Ext.toLowerCase() +
                    ";base64," +
                    resp.Datadoc +
                    "";
                  var o = document.createElement("a");
                  var i = resp.Descr;
                  o.href = a;
                  o.download = i;
                  o.click();
                },
                error: function (e) {
                  console.log(e);
                },
              }
            );
          }
        } else {
          o.show("Select an item to download");
        }
      },
      onUploadComplete: function (e) {
        var t = this;
        var a = this.getView().getBindingContext();
        var i = a.getPath();
        var currentObject = a.getModel().getProperty(i);
        var n = e.getParameter("files")[0].fileName;
        var l = {};
        l.Datadoc = this.arrayBufferToBase64(currentFile);
        l.Filename = n;
        l.Bukrs = currentObject.Bukrs;
        l.Livello = currentObject.Livello;
        l.Belnr = currentObject.Belnr;
        l.Gjahr = currentObject.Gjahr;
        l.Descr = n;
        var that = this;

        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.create("/DocumentoSet", l, {
          method: "POST",
          success: function (e) {
            console.log(e);
            window.location.reload();
          },
          error: function (e) {
            console.log(e);
          },
        });
      },

      handleUploadComplete: function (e) {
        var t = this;
        var a = this.getView().getBindingContext();
        var i = a.getPath();
        var currentObject = a.getModel().getProperty(i);

        var l = {};
        l.Datadoc = this.arrayBufferToBase64(currentFile.content);
        l.Filename = currentFile.fileName;
        l.Bukrs = currentObject.Bukrs;
        l.Livello = currentObject.Livello;
        l.Belnr = currentObject.Belnr;
        l.Gjahr = currentObject.Gjahr;
        l.Descr = currentFile.fileName;
        var that = this;
        console.log(l);
        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.create("/DocumentoSet", l, {
          method: "POST",
          success: function (e) {
            console.log(e);

            that.getOwnerComponent().getModel().refresh();
          },
          error: function (e) {
            console.log(e);
          },
        });
      },

      handleUploadPress: function (e) {
        var oFileUploader = this.byId("fileUploader");
        var fileName = e.getParameter("fileName");
        console.log(fileName);

        oFileUploader.upload();

        oFileUploader.clear();

        /*
            oFileUploader.checkFileReadable().then(function() {
                oFileUploader.upload();
			}, function(error) {
				MessageToast.show("The file cannot be read. It may have changed.");
			}).then(function() {
			});
*/
      },

      arrayBufferToBase64: function (e) {
        var t = "";
        var a = new Uint8Array(e);
        var o = a.byteLength;
        for (var i = 0; i < o; i++) {
          t += String.fromCharCode(a[i]);
        }
        return window.btoa(t);
      },

      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * Binds the view to the object path and expands the aggregated line items.
       * @function
       * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
       * @private
       */
      _onObjectMatched: function (oEvent) {
        console.log(oEvent.getParameters());

        var Belnr = oEvent.getParameter("arguments").Belnr;
        var Gjahr = oEvent.getParameter("arguments").Gjahr;
        var Bukrs = oEvent.getParameter("arguments").Bukrs;
        var Buzei = oEvent.getParameter("arguments").Buzei;
        var Livello = oEvent.getParameter("arguments").Livello;

        var oObject = {
          Belnr: Belnr,
          Gjahr: Gjahr,
          Bukrs: Bukrs,
          Livello: Livello,
        };

        //  this.setCurrentObj(oObject);
        this.getModel("appView").setProperty(
          "/layout",
          "TwoColumnsMidExpanded"
        );
        this.getModel()
          .metadataLoaded()
          .then(
            function () {
              var sObjectPath = this.getModel().createKey("FidochSet", {
                Belnr: Belnr,
                Bukrs: Bukrs,
                Gjahr: Gjahr,
                Buzei: Buzei,
                Livello: Livello,
              });
              this._bindView("/" + sObjectPath);
              this.setCurrentPath(sObjectPath);
            }.bind(this)
          );
      },

      /**
       * Binds the view to the object path. Makes sure that detail view displays
       * a busy indicator while data for the corresponding element binding is loaded.
       * @function
       * @param {string} sObjectPath path to the object to be bound to the view.
       * @private
       */
      _bindView: function (sObjectPath) {
        // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
        var oViewModel = this.getModel("detailView");

        oViewModel.setProperty("/busy", false);

        this.getView().bindElement({
          path: sObjectPath,
          events: {
            change: this._onBindingChange.bind(this),
            dataRequested: function () {
              oViewModel.setProperty("/busy", true);
            },
            dataReceived: function () {
              oViewModel.setProperty("/busy", false);
            },
          },
        });
        console.log("_bindView");
        // Set busy indicator during view binding
        /*
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath);
        console.log(oObject);
        this.setCurrentObj(oObject);
*/
      },

      _onBindingChange: function () {
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        // No data for the binding
        if (!oElementBinding.getBoundContext()) {
          this.getRouter().getTargets().display("detailObjectNotFound");
          // if object could not be found, the selection in the master list
          // does not make sense anymore.
          this.getOwnerComponent().oListSelector.clearMasterListSelection();
          return;
        }

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          //sObjectId = oObject.Belnr,
          sObjectName = oObject.Bukrs,
          oViewModel = this.getModel("detailView");
        var Belnr = oObject.Belnr;
        var Gjahr = oObject.Gjahr;
        var Bukrs = oObject.Bukrs;
        var Buzei = oObject.Buzei;
        this.setCurrentObj(oObject);
        this.getOwnerComponent().oListSelector.selectAListItem(sPath);

        /*
			oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage",
                oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
*/
        var that = this;
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.read(sPath + "/FidociSet", {
          success: function (oRetrievedResult) {
            var oViewModel = that.getModel("detailView");
            oViewModel.setProperty(
              "/CountFidoci",
              oRetrievedResult.results.length
            );
            that.setPosizioni(oRetrievedResult.results);
          },
          error: function (oError) {
            console.log(oError);
          },
        });
        oModelData1.read(sPath + "/DocumentoSet", {
          success: function (oRetrievedResult) {
            var oViewModel = that.getModel("detailView");
            oViewModel.setProperty(
              "/CountDocumento",
              oRetrievedResult.results.length
            );
          },
          error: function (oError) {
            console.log(oError);
          },
        });
        oModelData1.read(sPath + "/CronologiahSet", {
          success: function (oRetrievedResult) {
            var oViewModel = that.getModel("detailView");
            oViewModel.setProperty(
              "/CountCronologia",
              oRetrievedResult.results.length
            );
          },
          error: function (oError) {
            console.log(oError);
          },
        });
      },

      _onMetadataLoaded: function () {
        // Store original busy indicator delay for the detail view
        var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
          oViewModel = this.getModel("detailView");

        // Make sure busy indicator is displayed immediately when
        // detail view is displayed for the first time
        oViewModel.setProperty("/delay", 0);

        // Binding the view will set it to not busy - so the view is always busy if it is not bound
        oViewModel.setProperty("/busy", true);
        // Restore original busy indicator delay for the detail view
        oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
      },

      /**
       * Set the full screen mode to false and navigate to master page
       */
      onCloseDetailPress: function () {
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          false
        );
        // No item should be selected on master after detail page is closed
        this.getOwnerComponent().oListSelector.clearMasterListSelection();
        this.getRouter().navTo("master");
        this.setCurrentObj(null);
        this.setCurrentPath(null);
      },

      /**
       * Toggle between full and non full screen mode.
       */
      toggleFullScreen: function () {
        var bFullScreen = this.getModel("appView").getProperty(
          "/actionButtonsInfo/midColumn/fullScreen"
        );
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          !bFullScreen
        );
        if (!bFullScreen) {
          // store current layout and go full screen
          this.getModel("appView").setProperty(
            "/previousLayout",
            this.getModel("appView").getProperty("/layout")
          );
          this.getModel("appView").setProperty(
            "/layout",
            "MidColumnFullScreen"
          );
        } else {
          // reset to previous layout
          this.getModel("appView").setProperty(
            "/layout",
            this.getModel("appView").getProperty("/previousLayout")
          );
        }
      },
      saveNota: function () {
        var that = this;
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding(),
          sPath = oElementBinding.getPath(),
          oObject = oView.getModel().getObject(sPath);
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {
          Bukrs: oObject.Bukrs,
          Belnr: oObject.Belnr,
          Gjahr: oObject.Gjahr,
          Livello: oObject.Livello,
          NotaDec: sap.ui.getCore().byId("idFragment--nota").getValue(),
        };
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.create("/CronologiahSet", object, {
          success: function (oRetrievedResult) {
            console.log(oRetrievedResult);
            sap.ui.getCore().byId("idFragment--nota").setValue("");

            that.closeDialogNota();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogNota();
          },
        });
      },
      openDialogNote: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogNote().open();
      },
      _getDialogNote: function () {
        if (!this._oDialogNote) {
          this._oDialogNote = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zfiwfapv.view.DialogNote",
            this
          );
          this.getView().addDependent(this._oDialogNote);
        }
        return this._oDialogNote;
      },

      closeDialogNota: function () {
        this._oDialogNote.close();
      },

         formatterDate: function (date, time) {
        // SAPUI5 formatters
        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
          pattern: "dd/MM/yyyy",
        });
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
          pattern: "KK:mm:ss a",
        });
        // timezoneOffset is in hours convert to milliseconds
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
        // format date and time to strings offsetting to GMT
        var dateStr = dateFormat.format(new Date(date.getTime() + TZOffsetMs)); //05-12-2012
        var timeStr = timeFormat.format(new Date(time.ms + TZOffsetMs)); //11:00 AM
        //parse back the strings into date object back to Time Zone
        var parsedDate = new Date(
          dateFormat.parse(dateStr).getTime() - TZOffsetMs
        ); //1354665600000
        var parsedTime = new Date(
          timeFormat.parse(timeStr).getTime() - TZOffsetMs
        ); //39600000

        console.log(dateStr + timeStr);
        return dateStr + "  " + timeStr;
      }
    });
  }
);
