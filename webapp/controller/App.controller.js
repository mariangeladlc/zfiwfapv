sap.ui.define(
  ["./BaseController", "sap/ui/model/json/JSONModel", "sap/m/MessageToast"],
  function (BaseController, JSONModel,MessageToast) {
    "use strict";
    var currentObj = null;

    return BaseController.extend("com.stulz.zfiwfapv.controller.App", {
      onInit: function () {
        var oViewModel,
          fnSetAppNotBusy,
          iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

        oViewModel = new JSONModel({
          busy: true,
          delay: 0,
          layout: "OneColumn",
          previousLayout: "",
          actionButtonsInfo: {
            midColumn: {
              fullScreen: false,
            },
          },
        });
        this.setModel(oViewModel, "appView");

        fnSetAppNotBusy = function () {
          oViewModel.setProperty("/busy", false);
          oViewModel.setProperty("/delay", iOriginalBusyDelay);
        };

        const width  = window.innerWidth || document.documentElement.clientWidth || 
        document.body.clientWidth;
        const height = window.innerHeight|| document.documentElement.clientHeight|| 
        document.body.clientHeight;

        console.log(width, height);
          oViewModel.setProperty("/width", width - width * 40 / 100 +"px");
          oViewModel.setProperty("/height", height - height * 40 / 100  /+"px");

        // since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(fnSetAppNotBusy);
        this.getOwnerComponent()
          .getModel()
          .attachMetadataFailed(fnSetAppNotBusy);

        // apply content density mode to root view
        this.getView().addStyleClass(
          this.getOwnerComponent().getContentDensityClass()
        );
      },
      _getDialogApprove: function () {
        if (!this._oDialogApprove) {
          this._oDialogApprove = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zfiwfapv.view.DialogApprove",
            this
          );
          this.getView().addDependent(this._oDialogApprove);
        }
        return this._oDialogApprove;
      },
      closeDialogApprove: function () {
        this._oDialogApprove.close();
      },
      openDialogApprove: function () {
        console.log(this.getCurrentObj());
        currentObj = this.getCurrentObj();
        if(currentObj != null && currentObj != undefined){
            var text = "Vuoi approvare : " + currentObj.Xblnr + "?";
            this._getDialogApprove().open();
        }else{
            MessageToast.show("Nessun elemento selezionato");
        }
        // sap.ui.getCore().byId("idFragment--fatturaApp").setText(text);
      },

      _getDialogFreeze: function () {
        if (!this._oDialogFreeze) {
          this._oDialogFreeze = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zfiwfapv.view.DialogFreeze",
            this
          );
          this.getView().addDependent(this._oDialogFreeze);
        }
        return this._oDialogFreeze;
      },
      closeDialogFreeze: function () {
        this._oDialogFreeze.close();
      },
      openDialogFreeze: function () {
        currentObj = this.getCurrentObj();
        if(currentObj != null && currentObj != undefined){
            var text = "Vuoi approvare : " + currentObj.Xblnr + "?";
            this._getDialogFreeze().open();
        }else{
            MessageToast.show("Nessun elemento selezionato");
        }

      },


      onCloseDetailPress: function () {
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          false
        );
        // No item should be selected on master after detail page is closed
        this.getOwnerComponent().oListSelector.clearMasterListSelection();
        this.getRouter().navTo("master");
                this.setDetail(null);
        this.setList(null);

      },

      approva: function () {
        var nota = sap.ui.getCore().byId("idFragment--richiesta_nota").getValue();
        var combo = sap.ui.getCore().byId("idFragment--comboApprove").getSelectedKey();
        var object ={
            Bukrs : this.getCurrentObj().Bukrs,
            Belnr : this.getCurrentObj().Belnr,
            Gjahr : this.getCurrentObj().Gjahr,
            Livello : this.getCurrentObj().Livello,
            Statusapv : "1",
            Causalecode : combo,
            NotaDec : nota,
            FidociSet : this.getPosizioni()
        }
        console.log(object);
        var that = this;
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.create("/ContainerSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");

            that.closeDialogApprove();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
            that.onCloseDetailPress();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogApprove();
          },
        });
    },



      freeze: function () {
        var nota = sap.ui.getCore().byId("idFragment--richiesta_nota_reject").getValue();
        var combo = sap.ui.getCore().byId("idFragment--comboFreeze").getSelectedKey();
        var object ={
            Bukrs : this.getCurrentObj().Bukrs,
            Belnr : this.getCurrentObj().Belnr,
            Gjahr : this.getCurrentObj().Gjahr,
            Livello : this.getCurrentObj().Livello,
            Statusapv : "3",
            Causalecode : combo,
            NotaDec : nota,
            FidociSet : this.getPosizioni()
        }
        console.log(object);
        var that = this;
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.create("/ContainerSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");

            that.closeDialogFreeze();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
              that.onCloseDetailPress();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogFreeze();
          },
        });
    },


});
  }
);
