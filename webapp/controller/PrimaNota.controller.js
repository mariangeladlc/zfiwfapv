sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "sap/ui/Device",
    "sap/ui/core/routing/History",
  ],
  function (
    BaseController,
    JSONModel,
    formatter,
    mobileLibrary,
    Device,
    History,
    UIComponent
  ) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("com.stulz.zfiwfapv.controller.PrimaNota", {
      formatter: formatter,

      /* =========================================================== */
      /* lifecycle methods                                           */
      /* =========================================================== */

      onInit: function () {
        // Model used to manipulate control states. The chosen values make sure,
        // detail page is busy indication immediately so there is no break in
        // between the busy indication for loading the view's meta data
        var oViewModel = new JSONModel({
          busy: false,
          delay: 0,
        });

        this.getRouter()
          .getRoute("primaNota")
          .attachPatternMatched(this._onObjectMatched, this);

        this.setModel(oViewModel, "primaNotaView");

        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(this._onMetadataLoaded.bind(this));
      },

      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * Binds the view to the object path and expands the aggregated line items.
       * @function
       * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
       * @private
       */
      _onObjectMatched: function (oEvent) {
        var Belnr = oEvent.getParameter("arguments").Belnr;
        var Gjahr = oEvent.getParameter("arguments").Gjahr;
        var Bukrs = oEvent.getParameter("arguments").Bukrs;
        var Buzei = oEvent.getParameter("arguments").Buzei;

        this.getModel()
          .metadataLoaded()
          .then(
            function () {
              var sObjectPath = this.getModel().createKey("FidociSet", {
                Belnr: Belnr,
                Bukrs: Bukrs,
                Gjahr: Gjahr,
                Buzei: Buzei,
              });
              this._bindView("/" + sObjectPath);
            }.bind(this)
          );
      },

      /**
       * Binds the view to the object path. Makes sure that detail view displays
       * a busy indicator while data for the corresponding element binding is loaded.
       * @function
       * @param {string} sObjectPath path to the object to be bound to the view.
       * @private
       */
      _bindView: function (sObjectPath) {
        // Set busy indicator during view binding
        var oViewModel = this.getModel("primaNotaView");

        // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
        oViewModel.setProperty("/busy", false);

        this.getView().bindElement({
          path: sObjectPath,
          events: {
            change: this._onBindingChange.bind(this),
            dataRequested: function () {
              oViewModel.setProperty("/busy", true);
            },
            dataReceived: function () {
              oViewModel.setProperty("/busy", false);
            },
          },
        });
      },

      _onBindingChange: function () {
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        // No data for the binding
        if (!oElementBinding.getBoundContext()) {
          this.getRouter().getTargets().display("detailObjectNotFound");
          // if object could not be found, the selection in the master list
          // does not make sense anymore.
          this.getOwnerComponent().oListSelector.clearMasterListSelection();
          return;
        }

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          //sObjectId = oObject.Belnr,
          sObjectName = oObject.Bukrs,
          oViewModel = this.getModel("primaNotaView");
        var Belnr = oObject.Belnr;
        var Gjahr = oObject.Gjahr;
        var Bukrs = oObject.Bukrs;

        this.getOwnerComponent().oListSelector.selectAListItem(sPath);
      },
      saveNotaPrimaNota: function () {
        var that = this;
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding(),
          sPath = oElementBinding.getPath(),
          oObject = oView.getModel().getObject(sPath);

        var header = this.getCurrentObj();
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {
          Bukrs: oObject.Bukrs,
          Belnr : oObject.Belnr,
          Gjahr : oObject.Gjahr,
          Buzei : oObject.Buzei,
          Livello : header.Livello,
          Posnr : oObject.Buzei,
          NotaDec : sap.ui.getCore().byId("idFragment--nota_prima").getValue(),
        };
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.create("/CronologiaItemSet", object, {
          success: function (oRetrievedResult) {
            sap.ui.getCore().byId("idFragment--nota_prima").setValue("");

            that.closeDialogNota();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialogNota();
          },
        });
      },

      closeDialogNota: function () {
        this._oDialogNote.close();
      },
      _onMetadataLoaded: function () {
        // Store original busy indicator delay for the detail view
        var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
          oViewModel = this.getModel("primaNotaView");

        // Make sure busy indicator is displayed immediately when
        // detail view is displayed for the first time
        oViewModel.setProperty("/delay", 0);

        // Binding the view will set it to not busy - so the view is always busy if it is not bound
        oViewModel.setProperty("/busy", true);
        // Restore original busy indicator delay for the detail view
        oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
      },

      onNavBack: function () {
        history.go(-1);
      },
      openDialogNotePrimaNota: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogNote().open();
      },
      _getDialogNote: function () {
        if (!this._oDialogNote) {
          this._oDialogNote = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zfiwfapv.view.DialogNotePrimaNota",
            this
          );
          this.getView().addDependent(this._oDialogNote);
        }
        return this._oDialogNote;
      },

      closeDialogNotaPrimaNota: function () {
        this._oDialogNote.close();
      },
    });
  }
);
